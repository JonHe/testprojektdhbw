import { Component, OnInit } from '@angular/core';
import {DataService, person} from "../data.service";
import {ActivatedRoute, Params, Router} from "@angular/router";

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
  constructor(private data: DataService ,private router: Router,
              private route: ActivatedRoute) { }
  personForename: string;
  personName:string;
  personSubscribed: person;
  ngOnInit() {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.personForename = params['forename'];
          this.personName = params['name'];
        }
      );
    this.data.getPerson(this.personForename, this.personName).subscribe(person => this.personSubscribed = person);
  }

}
