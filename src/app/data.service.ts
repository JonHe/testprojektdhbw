import { Injectable } from '@angular/core';
import {Observable, of} from 'rxjs';


const PERSONS: person[] = [
  {name:'abc', forename:'abc', email:'abc@abc.de', number:'0010'},
];

export interface person {
  name: string;
  forename: string;
  number: string;
  email: string;
}
@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor() { }
  getPersons(): Observable<person[]> {
    return of(PERSONS);
  }
  addPerson(person: person){
    PERSONS.push(person);
  }
  getPerson(personForename, personName): Observable<person> {
   return of( PERSONS.find(o => o.forename === personForename && o.name ===personName));


  }
}
