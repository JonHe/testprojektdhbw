import { Component, OnInit } from '@angular/core';
import {DataService, person} from "../data.service";

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.css']
})
export class OverviewComponent implements OnInit {
  constructor(private data: DataService ) { }
  personArray: person[];
  newPerson:person = {forename: "max", name:"max", number:"00000", email:"asdf@asdf.net"};
  ngOnInit() {
    this.data.getPersons().subscribe(persons => this.personArray = persons);
  }
  createNewPerson(){
    var copyOfPerson:person = {forename:this.newPerson.forename, name:this.newPerson.name, number:this.newPerson.number, email:this.newPerson.email };
    this.data.addPerson(copyOfPerson);
  }
}
